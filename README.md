# VideoMixer
This project contains a setup for one specific live video performance called "everybody. welcome" ([see video](https://vimeo.com/365541763)), which was played at 2019 Live Performers Meeting in Rome.

Repository contains:

* VideoMixer.toe - the main project file
* toxes - folder with external toxes
* scripts - folder with externalized text files: python scripts, config tables and paths to operators.
* assets - folder with videos and pictures for the performance
* settings.py - specifies location of video assets and output resolution
* everybody_welcome_controls.touchosc - a layout file for touchOSC app, which is used to control the performance

To use the project you will need to configure [TouchOSC app](https://hexler.net/products/touchosc) on some mobile device.
If you do not wish to do so you can try the version of the project that uses only project internal UI. Please go to "performance_with_TD_UI_controls" branch and downlod the project from there.
To configure TouchOSC App, load everybody_welcome_controls.touchosc layout into the app. Connect your mobile device to the same network as the machine that runs the project.
Write corresponding ip addresses and ports in the network_config DAT in the root of the project and inside TouchOSC App.

## VideoMixer.toe
The project consists of the following external components:

### audio_module
Loads an audiofile, averages stereo channels and combines them into one.

The component outputs RMS power of high, mid and low frequencies of the audio channel, as well as RMS power of those frequencies after normalization and quantization, which works better for some effects.

NB: For performance audio should be played outside of TD project to avoid audio hiccups when framerate drops. It is possible to bring it in for auido analysis through Audiodevin CHOP and virtual audio cable https://www.vb-audio.com/Cable/.

### player
Contains:

* UI for video selection (container_player_window2). This component preloads assets from disk but only starts playing a video if its thumbnail is clicked.
* Video Out container (container_final_mix1). Sends final video mix to the secondary monitor.
* 4 components with TOP networks for compositing videos selected by the VJ in a specific way. In each of the four parts of the performance a different mixing network (container_composition*_mixing_layout) is used.

### content
Computer generated audio-reactive effects used in the preformance.

* base_waves analize audio wave as a spectrum and convert it into a gradient image through a glsl shader.
* base_circles draws disappearing ripples at every touch/click on the UI panel.

Scripts used in these components are located in /scripts/content folder.

### OSC_control

TouchOSC layout developed for the performance has 4 tabs.

![](https://bitbucket.org/kaleidoscopemisha/videomixer/raw/347ea4f6b7c3b7ddf131d16bd4b93f420ebe8a81/assets/pics/TouchOSC_layout_example.png)

Each tab accomodates controls for one part of the performance. Accordingly, all OSC data coming into TD is split into 4 parts. Each base_comp*_controls component inside OSC_control handles sliders and buttons from just one tab of the TouchOSC layout.

Pressing GO button on each tab will send send slider pereset values to TouchOSC app and to /OSC_control/oscin1 chop so that at the beginning of each part sliders are in correct positions. GO button will also point Video Out select OP (/player/container_final_mix1) to the output of one of the video compositing networks (/player/container_composition*_mixing_layout).

OSC_control component takes network info from network_config dat located in the root.

### modules
Located inside Local directory, it contains paths to different operators and is used for centralized referencing.



