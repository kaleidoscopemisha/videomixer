#paths to CHOPs that get OSC signals from TouchOSC app

#chops that control mixing of part 1 of the performance
#sliders
wave_height_comp1 = op('/OSC_control/base_comp1_controls/select_comp1')['comp1/wave_height']
wave_width_comp1 = op('/OSC_control/base_comp1_controls/select_comp1')['comp1/wave_width']
master_opacity_comp1 = op('/OSC_control/base_comp1_controls/select_comp1')['comp1/master_opacity']
flash_sensitivity_comp1 = op('/OSC_control/base_comp1_controls/select_comp1')['comp1/flash_sensitivity']
fb_opacity = op('/OSC_control/base_comp1_controls/select_comp1')['comp1/fb_opacity']
tree_opacity = op('/OSC_control/base_comp1_controls/select_comp1')['comp1/tree_opacity']


#buttons
stencil_choice = op('/OSC_control/base_comp1_controls/null_stencil_choice')['chan1']
green_blue_preset = op('/OSC_control/base_comp1_controls/select_comp1')['comp1/green_blue_preset']
BW_preset = op('/OSC_control/base_comp1_controls/select_comp1')['comp1/BW_preset']
blue_shift = op('/OSC_control/base_comp1_controls/select_comp1')['comp1/blue_shift']
flash_period = op('/OSC_control/base_comp1_controls/null_flash_period')['chan1']

#part 2 controls
#sliders
wave_height_comp2 = op('/OSC_control/base_comp2_controls/select_comp2')['comp2/wave_height']
wave_width_comp2 = op('/OSC_control/base_comp2_controls/select_comp2')['comp2/wave_width']
master_opacity_comp2 = op('/OSC_control/base_comp2_controls/select_comp2')['comp2/master_opacity']
flash_sensitivity_comp2 = op('/OSC_control/base_comp2_controls/select_comp2')['comp2/flash_sensitivity']
lower_channel_video1_opacity = op('/OSC_control/base_comp2_controls/select_comp2')['comp2/lower_channel_video1_opacity']
lower_channel_video2_opacity_comp2 = op('/OSC_control/base_comp2_controls/select_comp2')['comp2/lower_channel_video2_opacity']
lower_channel_opacity_comp2 = op('/OSC_control/base_comp2_controls/select_comp2')['comp2/lower_channel_opacity']
upper_channel_opacity_comp2 = op('/OSC_control/base_comp2_controls/select_comp2')['comp2/upper_channel_opacity']
rb_cross = op('/OSC_control/base_comp2_controls/select_comp2')['comp2/rb_cross']


#buttons
lower_matte_in2_choice = op('/OSC_control/base_comp2_controls/null_lower_matte_in2_choice')['chan1']
lower_matte_stencil_choice = op('/OSC_control/base_comp2_controls/null_lower_matte_stencil_choice')['chan1']
flash_invert = op('/OSC_control/base_comp2_controls/select_comp2')['comp2/flash_invert']
purple_preset = op('/OSC_control/base_comp2_controls/select_comp2')['comp2/purple_preset']

#part 3 controls
#sliders
waves_saturation = op('/OSC_control/base_comp3_controls/select_comp3')['comp3/waves_saturation']
triangles_caleido_cross = op('/OSC_control/base_comp3_controls/select_comp3')['comp3/triangles_caleido_cross']
lower_channel_video2_opacity_comp3 = op('/OSC_control/base_comp3_controls/select_comp3')['comp3/lower_channel_video2_opacity']
upper_channel_opacity_comp3 = op('/OSC_control/base_comp3_controls/select_comp3')['comp3/upper_channel_opacity']
th_channel_opacity_comp3 = op('/OSC_control/base_comp3_controls/select_comp3')['comp3/third_channel_opacity']
master_opacity_comp3 = op('/OSC_control/base_comp3_controls/select_comp3')['comp3/master_opacity']
flash_sensitivity_comp3 = op('/OSC_control/base_comp3_controls/select_comp3')['comp3/flash_sensitivity']


#buttons
lower_vid1_vid2_mixing_mode = op('/OSC_control/base_comp3_controls/null_lower_vid1_vid2_mixing_mode')['chan1']

#part 4 controls
#sliders
wave_height_comp4 = op('/OSC_control/base_comp4_controls/select_comp4')['comp4/wave_height']
wave_width_comp4 = op('/OSC_control/base_comp4_controls/select_comp4')['comp4/wave_width']
flash_sensitivity_comp4 = op('/OSC_control/base_comp4_controls/select_comp4')['comp4/flash_sensitivity']
lower_channel_opacity_comp4 = op('/OSC_control/base_comp4_controls/select_comp4')['comp4/lower_channel_opacity']
upper_channel_opacity_comp4 = op('/OSC_control/base_comp4_controls/select_comp4')['comp4/upper_channel_opacity']
th_channel_opacity_comp4 = op('/OSC_control/base_comp4_controls/select_comp4')['comp4/third_channel_opacity']
master_opacity_comp4 = op('/OSC_control/base_comp4_controls/select_comp4')['comp4/master_opacity']
bamboo_waves_cross = op('/OSC_control/base_comp4_controls/select_comp4')['comp4/bamboo_waves_cross']
bamboo_opacity = op('/OSC_control/base_comp4_controls/select_comp4')['comp4/bamboo_opacity']
saturation_bamboo_stencil = op('/OSC_control/base_comp4_controls/select_comp4')['comp4/saturation_bamboo_stencil']
flower_opacity = op('/OSC_control/base_comp4_controls/select_comp4')['comp4/flower_opacity']

#buttons
middle_matte_in1 = op('/OSC_control/base_comp4_controls/null_middle_matte_in1')['chan1']
middle_matte_stencil = op('/OSC_control/base_comp4_controls/null_middle_matte_stencil')['chan1']
circles = op('/OSC_control/base_comp4_controls/select_comp4')['comp4/circles']

#panels
circle_pad_v = op('/OSC_control/base_comp4_controls/select_comp4')['comp4/circle_pad1']
circle_pad_u = op('/OSC_control/base_comp4_controls/select_comp4')['comp4/circle_pad2']