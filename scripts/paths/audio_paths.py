#paths to audio analysis
#3 bars lo, mid, hi
bars = '/audio_module/analysis/out1'
#2 channel mic audiowave unprocessed
stream = '/audio_module/null_final'
#progression of hi, mid, lo in time
trail= '/audio_module/analysis/trail1'
#lo wave
lo_wave = '/audio_module/analysis/audiofilter_lo'
#mid wave
mid_wave = '/audio_module/analysis/audiofilter_mid'
#hi wave
high_wave = '/audio_module/analysis/audiofilter_hi'
#2 channel stereo wave averaged into 1 channel
stream_1ch = '/audio_module/analysis/null1'

#paths to various beat and lfo ops
beat_slideshow = '/audio_module/beat'
lfo_caleido = '/audio_module/lfo_caleido'

#path to normalized hi wave
normalized_hi = '/audio_module/analysis/normalized_hi'
normalized_mid = '/audio_module/analysis/normalized_mid'
normalized_lo = '/audio_module/analysis/normalized_lo'
