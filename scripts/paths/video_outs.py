#paths to video of background and overlay layers
bkgr_out = '/player/container_player_window2/container1/panel1/screens1/out_screen_1/null1'
overlay_out = '/player/container_player_window2/container1/panel2/screens1/out_screen_1/null1'

#paths to generated content
wave_out = '/content/base_waves/out1'
ramp_out = '/content/base_ramp/out1'
caleido_1_out = '/content/base_caleido_preset1/out1'
caleido_2_out = '/content/base_caleido_preset2/out1'

#paths to outs of each composition
comp_1_out = '/player/container_composition1_mixing_layout/out1'
comp_2_out = '/player/container_composition2_mixing_layout/out1'
comp_3_out = '/player/container_composition3_mixing_layout/out1'
comp_4_out = '/player/container_composition4_mixing_layout/out1'
