# me - this DAT
# 
# channel - the Channel object which has changed
# sampleIndex - the index of the changed sample
# val - the numeric value of the changed sample
# prev - the previous sample value
# 
# Make sure the corresponding toggle is enabled in the CHOP Execute DAT.

def onOffToOn(channel, sampleIndex, val, prev):
	out_op = op(me.mod.video_outs.comp_2_out)
	selector = op('/player/container_final_mix1/select1')
	selector.par.top = out_op
	for row in range ( 1, op('table2').numRows ):
		op('oscout1').sendOSC( op('table2')[row,0], [op('table2')[row,1]] )
		op('oscout2').sendOSC( op('table2')[row,0], [float(op('table2')[row,1])] )

	#select sliders from the composition to control flash sensitivity etc.
	flash_sensitivity_control = op('/content/base_flash/math2')
	flash_sensitivity_control.par.gain.expr = 'me.mod.OSC_paths.flash_sensitivity_comp2'

	wave_width_control = op('/content/base_waves/base_spectrum_top/audiospect2')
	wave_width_control.par.outlength.expr = 'me.mod.OSC_paths.wave_width_comp2'
	
	wave_height_control = op('/content/base_waves/base_spectrum_top/math1')
	wave_height_control.par.gain.expr = 'me.mod.OSC_paths.wave_height_comp2'