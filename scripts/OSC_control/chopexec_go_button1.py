#script to execute on the push of go button

def onOffToOn(channel, sampleIndex, val, prev):

	#change performance's output screen to the composition's out screen
	out_op = op(me.mod.video_outs.comp_1_out)
	selector = op('/player/container_final_mix1/select1')
	selector.par.top = out_op
	
	#send slider pereset values to OSCtouch app and to TD OSC in chop
	for row in range ( 1, op('table2').numRows ):
		#sent to TouchOSC app
		op('oscout1').sendOSC( op('table2')[row,0], [op('table2')[row,1]] )
		#send to oscin1 CHOP
		op('oscout2').sendOSC( op('table2')[row,0], [float(op('table2')[row,1])] )
	
	#select sliders from the composition to control flash sensitivity etc.
	flash_sensitivity_control = op('/content/base_flash/math2')
	flash_sensitivity_control.par.gain.expr = 'me.mod.OSC_paths.flash_sensitivity_comp1'

	wave_width_control = op('/content/base_waves/base_spectrum_top/audiospect2')
	wave_width_control.par.outlength.expr = 'me.mod.OSC_paths.wave_width_comp1'
	
	wave_height_control = op('/content/base_waves/base_spectrum_top/math1')
	wave_height_control.par.gain.expr = 'me.mod.OSC_paths.wave_height_comp1'