#when null_click CHOP changes value
#update replication table

def onValueChange(channel, sampleIndex, val, prev):
	index = int(op('count1')['select'])
	tx = op('null_tx')['tx']
	ty = op('null_ty')['ty']
	click = int(op('null_click')['select'])
	table = op('table1')
	table[index,0] = tx
	table[index,1] = ty
	table[index,2] = click

	