//shader for content/base_waves component
//converts CHOP wave into a picture
//where part below the wave is filled with a gradient color

#define AUDIO_SAMPLES (1280)
#define NB_SAMPLES	(1)
#define NB_BARS (AUDIO_SAMPLES / NB_SAMPLES)

#define SPACE		(0.15)

uniform samplerBuffer chan1;

const float NB_BARS_F = float(NB_BARS);
const float NB_SAMPLES_F = float(NB_SAMPLES);

out vec4 fragColor;
void main()
{
	vec4 bgColor = texture(sTD2DInputs[0], vUV.st);

	int bar = int(floor(vUV.s * NB_BARS_F));
    float f = 0.0;
    f = 0.0;
    
    for(int t=0; t<NB_SAMPLES; t++)
    {
    	f += texelFetch(chan1, bar*NB_SAMPLES+t).r;
    }
    f /= NB_SAMPLES_F;
    f *= 1.0;
    f += 0.0;

    vec4 barsColor = vec4(1.0, 1.0, 1.0, 1.0); // Currently using a fixed bars color
    vec4 finalColor = f <= vUV.y ? bgColor : mix(bgColor, barsColor, 1.0 - (vUV.y / f));

    float bar_f = float(bar)/NB_BARS_F;
    float val = (min(
            	(vUV.x-bar_f)*NB_BARS_F,
            	1.-(vUV.x-bar_f)*NB_BARS_F)
         	-SPACE*.5)/NB_BARS_F * uTDOutputInfo.res.b;
    fragColor = val < 0.0 ? bgColor : finalColor;

}
	