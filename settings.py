#directory with visuals for various performances
visualsPath = 'assets/visuals'

#name of folder with visulas for current perfomance
#it should contain folders called "bkgr" and "overlay"
performanceFolder = 'LPM2019'

#resolution
resX = 1280
resY = 720
